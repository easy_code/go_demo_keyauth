package auth

import (
	"gitee.com/easy_code/go_demo_keyauth/apps/audit"
	"gitee.com/easy_code/go_demo_keyauth/apps/authpolicy"
	"gitee.com/easy_code/go_demo_keyauth/apps/token"
	"gitee.com/easy_code/go_demo_keyauth/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewKeyauthAuther(client *rpc.ClientSet, serviceName string) *KeyauthAuther {
	return &KeyauthAuther{
		auth:        client.Token(),
		perm:        client.Policy(),
		audit:       client.Audit(),
		log:         zap.L().Named("http.auther"),
		serviceName: serviceName,
	}
}

// 有Keyauth提供的 HTTP认证中间件
type KeyauthAuther struct {
	log         logger.Logger
	auth        token.ServiceClient
	perm        authpolicy.RPCClient
	audit       audit.RPCClient
	serviceName string
}
