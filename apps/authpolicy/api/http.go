package api

import (
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"

	"gitee.com/easy_code/go_demo_keyauth/apps/authpolicy"
)

var (
	h = &handler{}
)

type handler struct {
	service authpolicy.Service
	log     logger.Logger
}

func (h *handler) Config() error {
	h.log = zap.L().Named(authpolicy.AppName)
	h.service = app.GetGrpcApp(authpolicy.AppName).(authpolicy.Service)
	return nil
}

func (h *handler) Name() string {
	return authpolicy.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{h.Name()}

	ws.Route(ws.POST("/").To(h.CreatePolicy).
		Doc("create a authpolicy").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(authpolicy.CreatePolicyRequest{}).
		Writes(response.NewData(authpolicy.Policy{})))
}

func init() {
	app.RegistryRESTfulApp(h)
}
