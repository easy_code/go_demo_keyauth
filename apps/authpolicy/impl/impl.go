package impl

import (
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"

	"gitee.com/easy_code/go_demo_keyauth/apps/authpolicy"
	"gitee.com/easy_code/go_demo_keyauth/apps/authrole"
	"gitee.com/easy_code/go_demo_keyauth/conf"
)

var (
	// Service 服务实例
	svr = &service{}
)

type service struct {
	col  *mongo.Collection
	log  logger.Logger
	role authrole.Service

	authpolicy.UnimplementedRPCServer
}

func (s *service) Config() error {
	// 依赖MongoDB的DB对象
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}

	// 获取一个Collection对象, 通过Collection对象 来进行CRUD
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())
	s.role = app.GetInternalApp(authrole.AppName).(authrole.Service)
	return nil
}

func (s *service) Name() string {
	return authpolicy.AppName
}

func (s *service) Registry(server *grpc.Server) {
	authpolicy.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryInternalApp(svr)
	app.RegistryGrpcApp(svr)
}
