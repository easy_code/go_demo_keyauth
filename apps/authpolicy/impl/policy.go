package impl

import (
	"context"

	"gitee.com/easy_code/go_demo_keyauth/apps/authpolicy"
	"gitee.com/easy_code/go_demo_keyauth/apps/authrole"
	"github.com/infraboard/mcube/exception"
)

func (s *service) ValidatePermission(ctx context.Context, req *authpolicy.ValidatePermissionRequest) (
	*authpolicy.Policy, error) {
	// 根据用户和命名空间找到该用户的授权策略
	// 由于使用分页, 只查询100条数据
	query := authpolicy.NewQueryPolicyRequest()
	query.Namespace = req.Namespace
	query.Username = req.Username
	query.Page.PageSize = 100
	set, err := s.QueryPolicy(ctx, query)
	if err != nil {
		return nil, err
	}

	// 获取用户的角色, 从策略中抽取出来
	roleNames := set.Roles()

	s.log.Debugf("found roles: %s", roleNames)

	// 通Role模块查询所有的Role对象
	queryRoleReq := authrole.NewQueryRoleRequestWithName(roleNames)
	queryRoleReq.Page.PageSize = 100
	roles, err := s.role.QueryRole(ctx, queryRoleReq)
	if err != nil {
		return nil, err
	}

	// 根据Role判断用户是否具有权限
	hasPerm, role := roles.HasPermission(authrole.NewPermissionRequest(req.Service, req.Resource, req.Action))
	if !hasPerm {
		return nil, exception.NewPermissionDeny("not permission access service %s resource %s action %s",
			req.Service,
			req.Resource,
			req.Action,
		)
	}

	p := set.GetPolicyByRole(role.Spec.Name)
	return p, nil
}

func (s *service) QueryPolicy(ctx context.Context, req *authpolicy.QueryPolicyRequest) (
	*authpolicy.PolicySet, error) {
	query := newQueryPolicyRequest(req)
	return s.query(ctx, query)
}

func (s *service) CreatePolicy(ctx context.Context, req *authpolicy.CreatePolicyRequest) (
	*authpolicy.Policy, error) {
	ins, err := authpolicy.NewPolicy(req)
	if err != nil {
		return nil, exception.NewBadRequest("validate create book error, %s", err)
	}

	if err := s.save(ctx, ins); err != nil {
		return nil, err
	}

	return ins, err
}
