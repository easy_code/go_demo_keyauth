package all

import (
	_ "gitee.com/easy_code/go_demo_keyauth/apps/user/api"
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/easy_code/go_demo_keyauth/apps/authpolicy/api"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/authrole/api"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/token/api"
)
