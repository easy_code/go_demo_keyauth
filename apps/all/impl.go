package all

import (
	_ "gitee.com/easy_code/go_demo_keyauth/apps/user/impl"
	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "gitee.com/easy_code/go_demo_keyauth/apps/audit/impl"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/authendpoint/impl"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/authpolicy/impl"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/authrole/impl"
	_ "gitee.com/easy_code/go_demo_keyauth/apps/token/impl"
)
