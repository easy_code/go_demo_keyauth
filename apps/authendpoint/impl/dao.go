package impl

import (
	"context"

	"gitee.com/easy_code/go_demo_keyauth/apps/authendpoint"

	"github.com/infraboard/mcube/exception"
)

// Save Object
func (s *service) save(ctx context.Context, set *authendpoint.EndpiontSet) error {
	// s.col.InsertMany()
	if _, err := s.col.InsertMany(ctx, set.ToDocs()); err != nil {
		return exception.NewInternalServerError("inserted service %s authendpoint document error, %s",
			set.Service, err)
	}
	return nil
}
