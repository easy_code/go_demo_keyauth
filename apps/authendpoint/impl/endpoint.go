package impl

import (
	"context"

	"gitee.com/easy_code/go_demo_keyauth/apps/authendpoint"
)

func (s *service) RegistryEndpoint(ctx context.Context, req *authendpoint.EndpiontSet) (
	*authendpoint.RegistryResponse, error) {
	if err := s.save(ctx, req); err != nil {
		return nil, err
	}
	resp := authendpoint.NewRegistryResponse()
	return resp, nil
}
