package api

import (
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"

	"gitee.com/easy_code/go_demo_keyauth/apps/authrole"
)

var (
	h = &handler{}
)

type handler struct {
	service authrole.Service
	log     logger.Logger
}

func (h *handler) Config() error {
	h.log = zap.L().Named(authrole.AppName)
	h.service = app.GetGrpcApp(authrole.AppName).(authrole.Service)
	return nil
}

func (h *handler) Name() string {
	return authrole.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{h.Name()}

	ws.Route(ws.POST("/").To(h.CreateRole).
		Doc("create a authrole").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(authrole.CreateRoleRequest{}).
		Writes(response.NewData(authrole.Role{})))
}

func init() {
	app.RegistryRESTfulApp(h)
}
