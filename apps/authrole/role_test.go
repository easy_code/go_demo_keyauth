package authrole_test

import (
	"testing"

	"gitee.com/easy_code/go_demo_keyauth/apps/authrole"
)

func TestHasPermission(t *testing.T) {
	set := authrole.NewRoleSet()

	r := &authrole.Role{
		Spec: &authrole.CreateRoleRequest{
			Permissions: []*authrole.Permission{
				{
					Service:  "cmdb",
					AllowAll: true,
				},
			},
		},
	}

	set.Add(r)

	perm, role := set.HasPermission(&authrole.PermissionRequest{
		Service:  "cmdb",
		Resource: "secret",
		Action:   "create",
	})

	t.Log(role)
	if perm != true {
		t.Fatal("has perm error")
	}
}
