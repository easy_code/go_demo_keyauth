package impl

import (
	"context"

	"gitee.com/easy_code/go_demo_keyauth/apps/authrole"
)

func (s *service) CreateRole(ctx context.Context, req *authrole.CreateRoleRequest) (
	*authrole.Role, error) {
	ins := authrole.NewRole(req)

	if err := s.save(ctx, ins); err != nil {
		return nil, err
	}

	return ins, nil
}

func (s *service) QueryRole(ctx context.Context, req *authrole.QueryRoleRequest) (
	*authrole.RoleSet, error) {
	query := newQueryRequest(req)
	return s.query(ctx, query)
}
